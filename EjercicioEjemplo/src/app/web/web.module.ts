import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { ListadoClientesComponent } from './pages/listado-clientes/listado-clientes.component';
import { EditarClientesComponent } from './pages/editar-clientes/editar-clientes.component';
import { GuardarClientesComponent } from './pages/guardar-clientes/guardar-clientes.component';
import { LoginServiceService } from './services/login-service.service';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [LoginComponent, RegistroComponent, ListadoClientesComponent, EditarClientesComponent, GuardarClientesComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  exports:[LoginComponent, RegistroComponent, ListadoClientesComponent, EditarClientesComponent
  ],
  providers:[LoginServiceService]
})
export class WebModule { }
