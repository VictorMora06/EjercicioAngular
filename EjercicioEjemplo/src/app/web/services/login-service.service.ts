import { Injectable } from '@angular/core';
import { Cliente } from '../pages/interfaces/cliente.interface';
import { Usuario } from '../pages/interfaces/usuario.interface';

@Injectable()

export class LoginServiceService {

  private _clientes:Cliente[]=[
    {nombre: "Pedro",apellido:"Lopez",email:"pedrolopez234@gmail.com", telefono:242323423, id:0},
    {nombre: "Alex",apellido:"Garcia",email:"alexgarcia234@gmail.com", telefono:34534534, id:1}
  ]
  private _usuarios:Usuario[]=[
    {
      username:"admin",
      password:"admin",
      email:"admin@gmail.com"
    },
    {
      username:"admin2",
      password:"admin2",
      email:"admin@gmail.com"
    }
  ]

  constructor() {
  }

  get clientes(){
    return this._clientes
  }

  get usuarios(){
    return this._usuarios
  }

  agregarCliente(cliente:Cliente):void{
    //Pillar el último id y sumarle 1
    var ultimoClienteiD = this._clientes[this._clientes.length -1].id;
    console.log("ultimo cliente id: ", ultimoClienteiD);

    //Modificar el id del cliente nuevo
    cliente.id = ultimoClienteiD+1;
    //Añadirlo al array
    this._clientes.push(cliente);

    console.log("lista de clientes", this._clientes);
  }

  agregarUsuario(varUsername,varEmail,varPassword){
    let nuevoUsuario:Usuario = {username:varUsername,email:varEmail,password:varPassword};
    this._usuarios.push(nuevoUsuario);
    console.log(this._usuarios);
  }

  public comprobarDatos(usuario:string, contraseña:string):boolean{

    for(let i = 0 ; i < this._usuarios.length ; i++){
      if(this._usuarios[i].username==usuario){
        if(this._usuarios[i].password==contraseña){
          return true;
        }
      }
    }
    return false;

  }

  //metodo para guardar datos en el local strore
  public guardarDatoStorage(key:string, value:string){

    localStorage.setItem(key,value);

  }
  //para recuperarlo
  public recuperarDatosStorage(key:string){

    return localStorage.getItem(key);
  }
  eliminar(id:number):void{

    let indexCliente:number = this._clientes.findIndex((value, _, __) =>{
      return value.id == id
    });
    this._clientes.splice(indexCliente,1)
  }

  actualizarCliente(cliente:Cliente,id:number){
    this._clientes[id].nombre = cliente.nombre
    this._clientes[id].apellido = cliente.apellido
    this._clientes[id].email = cliente.email
    this._clientes[id].telefono = cliente.telefono
  }


}
