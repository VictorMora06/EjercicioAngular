import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../../services/login-service.service';
import { Cliente } from '../interfaces/cliente.interface';

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.css']
})
export class ListadoClientesComponent implements OnInit {

  clientes:Cliente[];

  constructor(private serviceLogin: LoginServiceService) { }

  ngOnInit(): void {
    this.clientes = this.serviceLogin.clientes
  }

  eliminar(id:number){
    this.serviceLogin.eliminar(id);
    this.clientes = this.serviceLogin.clientes
    console.log(this.serviceLogin.clientes)

  }

}
