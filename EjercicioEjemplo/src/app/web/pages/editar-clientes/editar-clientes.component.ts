import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../services/login-service.service';
import { Cliente } from '../interfaces/cliente.interface';

@Component({
  selector: 'app-editar-clientes',
  templateUrl: './editar-clientes.component.html',
  styleUrls: ['./editar-clientes.component.css']
})
export class EditarClientesComponent {


  path:String [] =[];
  clientes:Cliente[] = [];
  clienteActualizar:Cliente;
  id:number=-1;

  constructor(private servicio:LoginServiceService,private router:Router) {
    this.clientes=servicio.clientes;
    this.path=this.router.url.split('/')
    this.id = Number(this.path[this.path.length-1])
    this.clienteActualizar = this.clientes[this.id];
    console.log(this.clientes[this.id])
  }



  actualizar():void{
    this.servicio.actualizarCliente(this.clienteActualizar,this.id);
  }

  redireccion():void{
    this.router.navigate(['/clientes'])
  }




}
