import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../services/login-service.service';
import { Cliente } from '../interfaces/cliente.interface';

@Component({
  selector: 'app-guardar-clientes',
  templateUrl: './guardar-clientes.component.html',
  styleUrls: ['./guardar-clientes.component.css']
})
export class GuardarClientesComponent implements OnInit {

  constructor( private servicio:LoginServiceService, private router: Router) { }

  ngOnInit(): void {
  }

  nuevo:Cliente = {
    nombre: "",
    apellido: "",
    telefono: 0,
    email: "",
    id: 0
  };

  nuevoCliente():void{
    console.log("formulario: ",this.nuevo);
    this.servicio.agregarCliente(this.nuevo);

    this.nuevo ={
      nombre: "",
      apellido: "",
      telefono: 0,
      email: "",
      id: 0
    };

    this.router.navigate(['/clientes']);
  }

}
