import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardarClientesComponent } from './guardar-clientes.component';

describe('GuardarClientesComponent', () => {
  let component: GuardarClientesComponent;
  let fixture: ComponentFixture<GuardarClientesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuardarClientesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardarClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
