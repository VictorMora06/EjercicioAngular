import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLinkActive } from '@angular/router';
import { LoginServiceService } from '../../services/login-service.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  username:string
  email:string
  password:string
  mostrarError:boolean = false;

  constructor(private servicioUsuario:LoginServiceService,private router:Router) { }

  ngOnInit(): void {
  }

  submit(){
    if(this.username!=undefined && this.email!=undefined && this.password!=undefined){
      this.servicioUsuario.agregarUsuario(this.username,this.email,this.password)
      this.router.navigate(['/clientes']);

    }else{
      this.mostrarError = true;
    }

  }

  getUsuarios(){
    return this.servicioUsuario.usuarios;
  }

}
