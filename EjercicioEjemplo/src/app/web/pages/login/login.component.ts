import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginServiceService } from '../../services/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario:string="";
  contrasena:string="";
  valor:boolean=false;


  constructor(private servicio: LoginServiceService, private router:Router) { }

  ngOnInit(): void {
  }

  public entrarLogin(){

    if(this.valor=this.servicio.comprobarDatos(this.usuario,this.contrasena)){
      //Añado al local storange
      this.servicio.guardarDatoStorage("usuarioActivo",this.usuario);

      this.router.navigate(['/home'])

    }


  }





}
