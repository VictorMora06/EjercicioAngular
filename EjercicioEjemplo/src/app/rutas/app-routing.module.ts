import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarClientesComponent } from '../web/pages/editar-clientes/editar-clientes.component';
import { GuardarClientesComponent } from '../web/pages/guardar-clientes/guardar-clientes.component';
import { ListadoClientesComponent } from '../web/pages/listado-clientes/listado-clientes.component';
import { LoginComponent } from '../web/pages/login/login.component';
import { RegistroComponent } from '../web/pages/registro/registro.component';

const routes: Routes = [
  {
    path:'',
    component:LoginComponent,
    pathMatch:'full'
  },

  {
    path:'home',
    component:ListadoClientesComponent
  },

  {
    path:'clientes',
    component:ListadoClientesComponent,
  },
  {
    path:'registro',
    component:RegistroComponent
  },
  {
    path:'clientes/nuevo',
    component:GuardarClientesComponent
  },
  {
    path:'clientes/:id',
    component:EditarClientesComponent
  },

  {
    path:'**',
    redirectTo:''
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
